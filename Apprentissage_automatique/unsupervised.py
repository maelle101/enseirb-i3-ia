import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.datasets import make_blobs
from sklearn.cluster import KMeans
from random import randint

X, y = make_blobs(n_samples=300, centers=4, cluster_std=0.60, random_state=0)
plt.scatter(X[:, 0], X[:, 1])
# plt.show()

m = X.shape[0]
n = X.shape[1]

nb_iter = 100

K = 4

centroids = np.array([]).reshape(n, 0)
for i in range(K):
    rand = randint(0, m - 1)
    centroids = np.c_[centroids, X[rand]]
print(centroids)

Output = {}
EuclidianDistance = np.array([]).reshape(m, 0)

for k in range(K):
    tempdist = np.sum((X - centroids[:, k]) ** 2, axis=1)
    EuclidianDistance = np.c_[EuclidianDistance, tempdist]

C = np.argmin(EuclidianDistance, axis=1) + 1
Y = {}
for k in range(K):
    Y[k + 1] = np.array([]).reshape(2, 0)
for i in range(m):
    Y[C[i]] = np.c_[Y[C[i]], X[i]]
for k in range(K):
    Y[k + 1] = Y[k + 1].T
for k in range(K):
    centroids[:, k] = np.mean(Y[k + 1], axis=0)

for i in range(nb_iter):
    EuclidianDistance  = np.array([]).reshape(m, 0)
    for k in range(K):
        
