import numpy as np
from enum import Enum, auto


class City(Enum):
    BORDEAUX = auto()
    PARIS = auto()
    NICE = auto()
    LYON = auto()
    NANTES = auto()
    BREST = auto()
    LILLE = auto()
    CLERMONT_FERRAND = auto()
    STRASBOURG = auto()
    POITIERS = auto()
    ANGERS = auto()
    MONTPELLIER = auto()
    CAEN = auto()


_CITIES = {City.BORDEAUX: (44.833333, -0.566667),
           City.PARIS: (48.8566969, 2.3514616),
           City.NICE: (43.7009358, 7.2683912),
           City.LYON: (45.7578137, 4.8320114),
           City.NANTES: (47.2186371, -1.5541362),
           City.BREST: (48.4, -4.483333),
           City.LILLE: (50.633333, 3.066667),
           City.CLERMONT_FERRAND: (45.783333, 3.083333),
           City.STRASBOURG: (48.583333, 7.75),
           City.POITIERS: (46.583333, 0.333333),
           City.ANGERS: (47.466667, -0.55),
           City.MONTPELLIER: (43.6, 3.883333),
           City.CAEN: (49.183333, -0.35)}


def listToDict(list: list):
    return {l[0]: l[1] for l in list}


def distance(a, b):
    (x1, y1), (x2, y2) = (a, b)
    return np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)


NBFOURMI = 100
NBITERATIONS = 100
COEFEVAPORATION = 0.8
Q = 1 / len(_CITIES)


class CitiesPath:
    def __init__(self, citiesDict: dict, path: list = None):
        self._nbCity = len(citiesDict)
        if path is not None:
            self._path = path.copy()
        else:
            self._path = list(citiesDict.keys())
        self._length = self.computeLength(citiesDict)

    def getLength(self):
        return self._length

    def getCity(self, index):
        assert (index < self._nbCity)
        return self._path[index]

    def computeLength(self, citiesDict: dict):
        length = 0
        currentCity = self._path[0]
        for city in self._path:
            length += distance(citiesDict[currentCity], citiesDict[city])
            currentCity = city
        return length


def goAnt() -> CitiesPath:
    pass


def getBestPath(Pheromones, cities):
    visited = []


cities = listToDict(_CITIES)
Phero = np.zeros(shape=(len(cities), len(cities)), dtype=np.float32)
for interation in range(NBITERATIONS):
    newPhero = np.zeros(shape=(len(cities), len(cities)), dtype=np.float32)
    for ant in range(NBFOURMI):
        path = goAnt()
        putPhero = Q / path.getLength()
        previousCity = 0
        for i in range(len(cities)):
            city = path.getCity(i)
            newPhero[previousCity, city] += putPhero
    Phero *= COEFEVAPORATION
    Phero += newPhero
