from excelReader import *
from random import randint


class ProjectAttribution:
    def __init__(self, nbStudent, nbProject, attr=None, random=False):
        self._nbStudent = nbStudent
        self._nbProject = nbProject
        if attr is not None:
            assert len(attr) == self._nbStudent, "Initialization of Attribution : Wrong size of attribution"
            assert self.checkOnePerStudent(), "Initialization of Attribution : Wrong project number"
            self._attribution = attr.copy()
        if random:
            self._attribution = [randint(0, self._nbProject) for i in range(self._nbStudent)]
        else:
            self._attribution = [-1] * nbStudent

    def reverseAttribution(self):
        reverse = []
        for i in range(self._nbProject):
            l = []
            for student, project in enumerate(self._attribution):
                if i == project:
                    l.append(student)
            reverse.append(l)

        return reverse

    def checkOnePerStudent(self):
        return all(0 <= attr < self._nbProject for attr in self._attribution)

    def fitness(self, pb: ProblemHelper):
        return 10 * self._twoPerProject() + 5 * self._mixedTeam(pb) + self._satisfaction(pb)

    def _twoPerProject(self):
        reverse = self.reverseAttribution()
        score = 0
        for students in reverse:
            if len(students == 2):
                score += 1
        return score

    def _mixedTeam(self, pb: ProblemHelper):
        reverse = self.reverseAttribution()
        score = 0
        for students in reverse:
            groups = [pb.getStudentGroup(i) for i in students]
            if "EMMK" in groups and "ENSC" in groups:
                score += 1
        return score

    def _satisfaction(self, pb: ProblemHelper):
        score = 0
        for student, project in enumerate(self._attribution):
            wish = pb.getWish(student, project)
            if wish == 1:
                score += 2
            if wish == 2:
                score += 1
        return score
