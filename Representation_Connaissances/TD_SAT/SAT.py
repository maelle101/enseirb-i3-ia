from random import choice


def rand_generator(m, n):
    vars = list(range(-n, n + 1))
    vars.remove(0)
    return [[choice(vars) for j in range(3)] for i in range(m)]


def check(val, cnf):
    for clause in cnf:
        bool = False
        for var in clause:
            if var < 0:
                bool |= not val[-var - 1]
            else:
                bool |= val[var - 1]
        if not bool:
            return False
    return True


def satSolver(n, cnf, val=[], a=1):
    res = [(i in val) for i in range(n)]
    if check(res, cnf):
        print(res)
        return True
    res2 = res.copy()
    res2[-1] = True
    if check(res2, cnf):
        print(res2)
        return True
    bool = False
    for j in range(a, n):
        val2 = val.copy()
        val2.append(a)
        bool |= satSolver(n, cnf, val=val2, a=a + 1)
        if bool:
            return True
        bool |= satSolver(n, cnf, val=val, a=a + 1)
        if bool:
            return True
    return False


n = 8
m = 1000
cnf = rand_generator(m, n)
print(cnf)
print(satSolver(n, cnf))
