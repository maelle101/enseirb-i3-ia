# First we need to print clauses

sudoku = [318, 517, 713, 812, 421, 134, 435, 736, 241, 345, 549, 943, 252, 359, 754, 856, 168, 566, 777, 879, 371, 677,
          972, 683, 297, 393, 591, 697, 798]


def value(x, y, z):
    return x * 100 + y * 10 + z


def printClause(clause):
    print(" ".join([str(v) for v in clause]))


def equals1a(arrayOfVars):
    printClause(arrayOfVars)
    for i, x in enumerate(arrayOfVars):
        for y in arrayOfVars[i + 1:]:
            printClause([-x, -y])


def allEquals1a(arrayOfVars):
    for a in arrayOfVars:
        for b in arrayOfVars:
            equals1a([value(a, b, z) for z in arrayOfVars])


def column(arrayOfVars):
    for a in arrayOfVars:
        for b in arrayOfVars:
            tab = []
            for i, x in enumerate(arrayOfVars):
                p = a * 100 + x * 10 + b
                tab.append(p)
            printClause(tab)
            for i, x in enumerate(arrayOfVars):
                p = a * 100 + x * 10 + b
                for y in arrayOfVars[i + 1:]:
                    q = a * 100 + y * 10 + b
                    printClause([-p, -q])


def lign(arrayOfVars):
    for a in arrayOfVars:
        for b in arrayOfVars:
            tab = []
            tab = []
            for i, x in enumerate(arrayOfVars):
                p = x * 100 + a * 10 + b
                tab.append(p)
            printClause(tab)
            for i, x in enumerate(arrayOfVars):
                p = x * 100 + a * 10 + b
                for y in arrayOfVars[i + 1:]:
                    q = y * 100 + a * 10 + b
                    printClause([-p, -q])


def square(arrayOfVars, size):
    for i in range(1, size + 1):
        for j in range(1, size + 1):
            for z in arrayOfVars:
                tab = []
                for x in range(3 * (i - 1), 3 * i):
                    for y in range(3 * (j - 1), 3 * j):
                        p = x * 100 + y * 10 + z
                        tab.append(p)
                printClause(tab)
                for x in range(3 * (i - 1), 3 * i):
                    for y in range(3 * (j - 1), 3 * j):
                        p = x * 100 + y * 10 + z
                        for a in range(x, 3 * i):
                            for b in range(3 * (j - 1), 3 * j):
                                if a != x and b!=y and (a > x or b > y):
                                    q = a * 100 + b * 10 + z
                                    printClause([-p,-q])


def sudokuData(sud):
    for c in sud:
        printClause([c])


size = 3
tmp = [x for x in range(1, size ** 2 + 1)]
allEquals1a(tmp)
column(tmp)
lign(tmp)
square(tmp, size)
sudokuData(sudoku)
