from sys import argv

from pysat.solvers import Solver

clues = []
solver = Solver()
for line in open(argv[1]):
    clue = [int(x) for x in line.split()]
    solver.add_clause(clue)
result = solver.solve()
print(result)
print(solver.get_model())